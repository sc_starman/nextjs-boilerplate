import React, { Component } from 'react'
import Link from 'next/link'
import withAuth from 'hooks/withAuth';

class Dashboard extends Component {
    constructor(props){
        super(props)
        this.state = {
            users: {}
        }
    }
    render() {
        return (
            <div>
                this is a private route
                <Link href="/dashboard">
                    <a>dashboard</a>
                </Link>
            </div>
        )
    }
}

export default withAuth(Dashboard)