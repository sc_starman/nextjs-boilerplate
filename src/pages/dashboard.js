import React, { Component } from 'react'
import Link from 'next/link'
import withAuth from 'hooks/withAuth';
import api from 'api';

class Dashboard extends Component {
    constructor(props){
        super(props)
        this.state = {
            users: {}
        }
    }
    componentDidMount() {
        api.get('/Users').then(a => this.setState({ users: a.data }))
    }
    render() {
        return (
            <div>
                this is a private route
                <Link href="/auth/logout">
                    <a>logout</a>
                </Link>
                <Link href="/anotherPage">
                    <a>antoher private page</a>
                </Link>
            </div>
        )
    }
}

export default withAuth(Dashboard)