import NProgress from 'nprogress'
import Router from 'next/router'

const initialProgressBar = () => {

    Router.events.on('routeChangeStart', url => {
        console.log(`Loading: ${url}`)
        NProgress.start()
    })
    Router.events.on('routeChangeComplete', () => NProgress.done())
    Router.events.on('routeChangeError', () => NProgress.done())
}

export default initialProgressBar