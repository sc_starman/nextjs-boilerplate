import axios from 'axios';
// configure base url
const api = axios.create({
    baseURL: '/api/',
});

// intercept requests and add authorization token
api.interceptors.request.use((config) => {
    config.withCredentials = true;
    return config;
});

    
export default api;
