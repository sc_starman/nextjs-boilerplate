import React, { Component } from 'react'

import { isAuthValid } from '../../shared/authHandler';

export default function withAuth(WrappedComponent) {
  return class Authenticated extends Component {

    static async getInitialProps({ req, res }) {

      if (typeof window === 'undefined' && !isAuthValid(req)) {
        return res.redirect('/auth/login')
      }

      // Check if Page has a `getInitialProps`; if so, call it.
      const pageProps = WrappedComponent.getInitialProps && await WrappedComponent.getInitialProps(ctx);
      // Return props.
      return {
        ...pageProps,
      }
    }
    render() {
      return (
        <WrappedComponent {...this.props} />
      )
    }
  }
}
