import { actionPending } from "store/utils/actionState";


const actionTypes = {
    CHANGE_TITLE: 'HomePage/CHANGE_TITLE',
}


export function changeTitle() {
    return {
        type: actionPending(actionTypes.CHANGE_TITLE)
    };
}

export default actionTypes