/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectTitle = () =>
  createSelector(
    selectHome,
    homeState => homeState.title,
  );

export { selectHome, makeSelectTitle };
