import produce from 'immer';
import { actionFulfilled } from "store/utils/actionState";

import { default as actionsTypes } from './actions';


export const REDUCER_KEY = "home"

export const initialState = {
    title: 'numa',
};

const homeReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case actionFulfilled(actionsTypes.CHANGE_TITLE):
                draft.title = action.payload;
                break;
        }
    });

export default homeReducer;