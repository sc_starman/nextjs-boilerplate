import { put, select, takeLatest } from 'redux-saga/effects';
import { default as actionTypes } from './actions';

import { actionPending, actionFulfilled, actionRejected, actionResult } from 'store/utils/actionState';


import { makeSelectTitle } from './selectors';


export function* changeTitleWorker() {
    try {

        const title = yield select(makeSelectTitle())

        const newTitle = title.split("").reverse().join("");

        yield put(actionResult(actionFulfilled(actionTypes.CHANGE_TITLE), newTitle))


    } catch (err) {
        yield put(actionResult(actionRejected(actionTypes.CHANGE_TITLE), err));
    }
}

export default function* () {
    yield takeLatest(actionPending(actionTypes.CHANGE_TITLE), changeTitleWorker);
}
