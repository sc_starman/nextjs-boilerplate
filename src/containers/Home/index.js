import React from 'react';
import PropTypes from 'prop-types';

import isTruthy from 'helpers/isTruthy';

//---REDUX---\\
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

//(INFERA)//
import { useInjectReducer } from 'store/utils/injectReducer';
import { useInjectSaga } from 'store/utils/injectSaga';

//(CONTAINER RELATED)//
import { changeTitle } from './actions';
import reducer, { REDUCER_KEY } from './reducer';
import saga from './saga';
import { makeSelectTitle } from './selectors';
import monitorSagas from 'store/utils/monitorSagas';
//---REDUX---\\------//---END---\\


export function HomePage({
  title,
  defaultTitle,
  onChangeTitle,
}) {

  useInjectReducer({ key: REDUCER_KEY, reducer });
  useInjectSaga({ key: REDUCER_KEY, saga });
  return (
    <article>
      <h1>{isTruthy(title) ? title : defaultTitle}</h1>
      <button onClick={onChangeTitle}>Reverse Title</button>
    </article>
  );
}


HomePage.propTypes = {
  defaultTitle: PropTypes.string,
};



const mapStateToProps = createStructuredSelector({
  title: makeSelectTitle(),
});


export function mapDispatchToProps(dispatch) {
  return {
    onChangeTitle: newTitle => dispatch(changeTitle(newTitle))
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
)(HomePage);