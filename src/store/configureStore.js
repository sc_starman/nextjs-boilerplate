/**
 * Create the store with dynamic reducers
 */

import { createStore } from 'redux';

import createSagaMiddleware from 'redux-saga';

import createReducers from './reducers';
import bindMiddleware from './middlewares';

export default function configureStore(initialState = {}) {

  const sagaMiddleware = createSagaMiddleware()

  const store = createStore(
    createReducers(),
    initialState,
    bindMiddleware([sagaMiddleware]),
  );

  // Extensions
  store.runSaga = sagaMiddleware.run;
  store.injectedReducers = {}; // Reducer registry
  store.injectedSagas = {}; // Saga registry

  return store;
}
