
export const actionState = {
    PENDING: "_PENDING",
    FULFILLED: "_FULFILLED",
    REJECTED: "_REJECTED",
}

export const createActionState = (type, actionType) => {
    switch (actionType) {
        case actionState.FULFILLED:
            return `${type}${actionState.FULFILLED}`
        case actionState.REJECTED:
            return `${type}${actionState.REJECTED}`
        default:
            return `${type}${actionState.PENDING}`
    }
}

export const actionPending = (type) => `${type}${actionState.PENDING}`
export const actionFulfilled = (type) => `${type}${actionState.FULFILLED}`
export const actionRejected = (type) => `${type}${actionState.REJECTED}`
export const actionResult = (type, payload) => ({ type, payload })