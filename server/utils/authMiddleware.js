import { isAuthValid } from "../../shared/authHandler";

export const authMiddleware = (req, res, next) => {

    if (!isAuthValid(req)) {
        return res.redirect('/auth/login')
    }
    else
        next()
}