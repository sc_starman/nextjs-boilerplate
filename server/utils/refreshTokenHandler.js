import getConfig from 'next/config';
import qs from 'qs'
import { getAccessToken, getRefreshToken, setAuth } from '../../shared/authHandler';
import fetch from 'fetch-retry'

export const refreshTokenHandler = (req, res) => {

    const { serverRuntimeConfig } = getConfig();
    const { AUTH, APP_GATEWAY_URL } = serverRuntimeConfig
    const { GATEWAY_URL, LOGIN_CALLBACK, CLIENT_ID, CLIENT_SECRET, TOKEN_ENDPOINT } = AUTH


    const refreshToken = getRefreshToken(req)
    const callback = {
        grant_type: 'refresh_token',
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        redirect_uri: `${APP_GATEWAY_URL}${LOGIN_CALLBACK}`,
        refresh_token: refreshToken
    }

    // const access_token = req.session.auth.access_token
    const access_token = getAccessToken(req)

    // Query API for token
    fetch(`${GATEWAY_URL}${TOKEN_ENDPOINT}`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': "Basic " + access_token
        },
        body: qs.stringify(callback)
    })
        .then(r => {
            return r.json()
        })
        .then(data => {
                setAuth(res,data)
        });
}