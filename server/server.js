import express from 'express';
import next from 'next';
import bodyParser from 'body-parser';
import csrf from 'csurf';
import helmet from 'helmet';
import compression from 'compression';
import getConfig from 'next/config';
import cookieParser from 'cookie-parser';

import authRoutes from './routes/auth'
import apiRoutes from './routes/api'
import { initialAuthSession } from '../shared/authHandler';


const __ENVIRONMENT__ = process.env.NODE_ENV !== 'production'

// Setup CSRF Middleware
const csrfProtection = csrf({ cookie: true });


const app = next({ dev: __ENVIRONMENT__, dir: './src', })
const handle = app.getRequestHandler()

app.prepare()
    .then(() => {

        const server = express()

        const { serverRuntimeConfig } = getConfig();

        const { PORT, COOKIE_SECRET } = serverRuntimeConfig


        initialAuthSession()

        const middlewares = [
            bodyParser.urlencoded({ extended: false }),
            cookieParser(COOKIE_SECRET),
            csrfProtection,
            helmet(),
            compression(),
        ]

        server.use(middlewares)
        ///routes configuration
        server.use(authRoutes, apiRoutes)

        server.get('*', (req, res) => handle(req, res))

        server.listen(parseInt(PORT, 10) || 3000, (err) => {
            if (err) throw err
            console.log(`> Ready on http://localhost:${parseInt(PORT, 10) || 3000}`)
        })
    })