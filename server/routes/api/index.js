import router from '../../utils/router';

import { getApiRouteHandler } from './get.route';
import { postApiRouteHandler } from './post.route';
import { putApiRouteHandler } from './put.route';
import { deleteApiRouteHandler } from './delete.route';
import { authMiddleware } from '../../utils/authMiddleware';

router.route('/api/*')
        .get(authMiddleware, getApiRouteHandler)
        .post(postApiRouteHandler)
        .put(putApiRouteHandler)
        .delete(deleteApiRouteHandler);

export default router