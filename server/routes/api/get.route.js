import fetch from 'fetch-retry'
import getConfig from 'next/config';


import { refreshTokenHandler } from '../../utils/refreshTokenHandler';
import { getAccessToken } from '../../../shared/authHandler';
import { extractApiRoute } from '../../utils';

export function getApiRouteHandler(req, res) {


    const { serverRuntimeConfig } = getConfig();
    const { API_GATEWAY_URL } = serverRuntimeConfig

    const accessToken = getAccessToken(req)
    const apiRoute = extractApiRoute(req.url)

    fetch(`${API_GATEWAY_URL}${apiRoute}`, {
        retryOn: (attempt, _error, response) => {
            // retry on any network error, or 4xx or 5xx status codes
            if (attempt < 3 && response.status === 401) {
                refreshTokenHandler(req, res)
                return true;
            }
            return false
        },
        retryDelay: (attempt, _error) => {
            return Math.pow(2, attempt) * 1000; // 1000, 2000, 4000
        },
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
        }
    })
        .then(response => {
            res.status(response.status)
            return response.json()
        })
        .then(response => res.json(response))
        .catch(err => res.status(502).json(err))

}