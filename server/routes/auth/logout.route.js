
import getConfig from 'next/config';
import { removeAuth } from '../../../shared/authHandler';

export default {
    logout,
    logoutCallback
}


function logout(req, res) {
    const { serverRuntimeConfig } = getConfig();
    const { AUTH, APP_GATEWAY_URL } = serverRuntimeConfig
    const {GATEWAY_URL, LOUGOUT_ENDPOINT, LOGOUT_CALLBACK}= AUTH

    removeAuth(res)

    return res.redirect(`${GATEWAY_URL}${LOUGOUT_ENDPOINT}?id_token_hint=${idToken}&post_logout_redirect_uri=${APP_GATEWAY_URL}${LOGOUT_CALLBACK}`)
}
function logoutCallback(req, res) {

    res.redirect('/dashboard?logout=true')
}

