import getConfig from 'next/config';


export function regiser(req, res) {
    const { serverRuntimeConfig } = getConfig();
    const { AUTH } = serverRuntimeConfig
    const { GATEWAY_URL, REGISTER_ENDPOINT } = AUTH
    res.redirect(`${GATEWAY_URL}${REGISTER_ENDPOINT}`)
}