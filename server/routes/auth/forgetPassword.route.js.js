import getConfig from 'next/config';


export function forgetPassword(req, res) {
    const { serverRuntimeConfig } = getConfig();
    const { AUTH } = serverRuntimeConfig
    const { GATEWAY_URL, FORGET_PASSWORD_URL } = AUTH
    res.redirect(`${GATEWAY_URL}${FORGET_PASSWORD_URL}`)
}