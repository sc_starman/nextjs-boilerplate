import router from '../../utils/router';

import loginRoutes from './login.route'
import logoutRoutes from './logout.route'
import { regiser } from './register.route'
import { forgetPassword } from './forgetPassword.route.js'
import { checkAuth } from './check.route';
import { authMiddleware } from '../../utils/authMiddleware';

router.get('/auth/login', loginRoutes.login)
router.get('/auth/login/callback', loginRoutes.loginCallback)
router.get('/auth/logout', logoutRoutes.logout)
router.get('/auth/logout/callback', logoutRoutes.logoutCallback)
router.get('/auth/register', regiser)
router.get('/auth/forgetPassword', forgetPassword)
router.get('/auth/check', authMiddleware, checkAuth)

export default router