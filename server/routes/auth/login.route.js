import getConfig from 'next/config';
import qs from 'qs'
import fetch from 'fetch-retry'
import { setAuth } from '../../../shared/authHandler';

export default {
    login,
    loginCallback
}

function login(req, res) {
    const { serverRuntimeConfig } = getConfig();
    const { AUTH, APP_GATEWAY_URL } = serverRuntimeConfig
    const { GATEWAY_URL, LOGIN_ENDPOINT, LOGIN_CALLBACK, CLIENT_ID, SCOPE } = AUTH

    
    res.redirect(`${GATEWAY_URL}${LOGIN_ENDPOINT}?client_id=${CLIENT_ID}&redirect_uri=${APP_GATEWAY_URL}${LOGIN_CALLBACK}&response_type=code&scope=${SCOPE}`)
}


function loginCallback(req, res) {
    const { serverRuntimeConfig } = getConfig();
    const { AUTH, APP_GATEWAY_URL } = serverRuntimeConfig
    const { GATEWAY_URL, LOGIN_CALLBACK, CLIENT_ID, CLIENT_SECRET, TOKEN_ENDPOINT } = AUTH

    const callback = {
        grant_type: "authorization_code",
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        redirect_uri: `${APP_GATEWAY_URL}${LOGIN_CALLBACK}`,
        code: req.query.code
    }

    // Query API for token
    fetch(`${GATEWAY_URL}${TOKEN_ENDPOINT}`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: qs.stringify(callback)
    })
        .then(r => {
            return r.json()
        })
        .then(data => {
            setAuth(res, data)
            res.redirect('/dashboard')
        });

}

