const { join } = require('path');

const paths = {
  src: join(__dirname, 'src'),
  server: join(__dirname, 'server'),
  static: join(__dirname, 'static'),
};

const alias = {
  static: paths.static,
  server: paths.server,
  api: join(paths.src, 'api'),
  utils: join(paths.src, 'utils'),
  pages: join(paths.src, 'pages'),
  components: join(paths.src, 'components'),
  containers: join(paths.src, 'containers'),
  store: join(paths.src, 'store'),
  hooks: join(paths.src, 'hooks'),
  helpers: join(paths.src, 'helpers'),
};

module.exports = {
  distDir: '../build',
  serverRuntimeConfig: {
    PORT: 3000,
    COOKIE_SECRET: "secret",
    AUTH: {
      GATEWAY_URL: "http://localhost:5000",
      CLIENT_ID: "nextjs-dashboard",
      CLIENT_SECRET: "fPnWF1mSeUNgRo8mkB+6uf0pn5E6ac4ciEuN+IpB6v4tjkVsd0Cci0ciovFvdcPVcdvYFYS2PpKVQnWFP2Avtw==",
      SCOPE: "email profile openid offline_access",
      LOGIN_ENDPOINT: "/connect/authorize",
      LOUGOUT_ENDPOINT: "/connect/endsession",
      TOKEN_ENDPOINT: "/connect/token",
      FORGET_PASSWORD_URL: "/Account/ForgotPassword",
      REGISTER_ENDPOINT: "/Account/Register",
      LOGIN_CALLBACK: "/auth/login/callback",
      LOGOUT_CALLBACK: "/auth/logout/callback"
    },
    API_GATEWAY_URL: "http://localhost:5001/api",
    APP_GATEWAY_URL: "http://localhost:3000",
  },
  publicRuntimeConfig: {
  },
  webpack(config, options) {
    config.module.rules.push(
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              fallback: 'file-loader',
              publicPath: '/_next/static/fonts/',
              outputPath: 'static/fonts/',
              name: '[name]-[hash].[ext]',
            },
          },
        ],
      },
      {
        test: /\.(jpe?g|png|svg|gif|ico)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              fallback: 'file-loader',
              publicPath: '/_next/static/images/',
              outputPath: `${options.isServer ? '../' : ''}static/images/`,
              name: '[name]-[hash].[ext]',
            },
          },
        ],
      },
    );

    config.resolve.alias = Object.assign(
      {},
      config.resolve.alias,
      alias,
    );

    return config;
  },
};