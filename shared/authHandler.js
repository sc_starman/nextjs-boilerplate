import uuidv4 from 'uuid/v4'

export const initialAuthSession = () => {
    global.authSession = {}
}

export const isAuthValid = (req) => {
    const sid = getSid(req)
    return validate(sid)
}

export const setAuth = (res, data) => {
    const authKey = uuidv4()
    res.cookie('sid', authKey, {
        secure: false,
        httpOnly: true,
        signed: true
    })
    global.authSession[authKey] = data
}

export const getAccessToken = (req) => {
    const sid = getSid(req)
    const authSession = global.authSession[sid]
    if (validate(sid))
        return authSession.access_token
}

export const getRefreshToken = (req) => {
    const sid = getSid(req)
    const authSession = global.authSession[sid]
    if (validate(sid))
        return authSession.refresh_token
}

export const removeAuth = (res) => {
    res.cookie('sid', '', { expires: new Date(0) })
}

const validate = (sid) => global.authSession[sid] && global.authSession[sid].hasOwnProperty('access_token')

const getSid = (req) => {
    return req.signedCookies.sid
}